#!/bin/zsh


for i in `ls | grep run`;
do
	cd $i
	echo "start $i"
	echo "re-seeding...";
	$TESTBED_BIN/seed-all.sh;
	echo "start simulation...";
	screen -d -m $TESTBED_BIN/run-all.sh
	cd -
done
