#!/bin/zsh

echo -n "id"
for i in {2..40}
do
	#echo -n ", $(($i/10.0))"
	echo -n ", $i"
done
echo -n "\n"

for i in `ls | grep NewT`;
do
	cd $i
	first=true
	echo -n "$i, "
	for j in `ls *.log.bz2 | sort -n`;
	do 
		if [ "$first" = false ] ; then
			echo -n ', '
		else
			first=false
		fi
      bunzip2 $j
      file="${j%.*}"
		$TESTBED_BIN/throughput.pl $file; 
      bzip2 $file
	done
	echo -n "\n"
	cd ..
done
