#!/usr/bin/perl

use strict;
use warnings;

my $first = -1;
my $last = -1;
my $count = 0;
my $maxnode = 0;
my $normalizer = 64/32; # slots per second
while (<>) {
	if (/time: (\d+)mote (\d+).*rx=\((\d+),(\d+)\).*/) {
		if ($first == -1) {
			$first = $1;
		}
 		$last = $1;		
		$count = $count + 1;
		if ($2 > $maxnode) {
			$maxnode = $2;
		}
	}
}
my $diff = $last - $first;
#print "diff: $diff, count: $count\n";
my $rate = $count/$diff*1000000;
my $noderate = $rate/(($maxnode-1)*0.75)/$normalizer/($maxnode);
#print "nodes: $maxnode\n";
print "$noderate";
